import binascii
from datetime import datetime
import os
import pytz

ISO_8601_FORMAT = "%Y-%m-%dT%H:%M:%S.%fZ"


def get_hex_string(size=12):
    return binascii.b2a_hex(os.urandom(size)).decode()


def base_headers(func):
    """
    Decorator that adds base headers to header kwarg.
    """
    def wrapper(inst, *args, **kwargs):
        hd_ = kwargs.get('headers', {})
        hd_.update(inst._base_headers)

        return func(inst, *args, **kwargs)

    return wrapper


def format_dt(dt):
    return dt.strftime(ISO_8601_FORMAT)


def parse_dt(date_str):
    try:
        dt = datetime.strptime(date_str, ISO_8601_FORMAT)
    except ValueError:
        dt = datetime.strptime(date_str, ISO_8601_FORMAT[:-4]+'Z')

    dt = dt.replace(tzinfo=pytz.UTC)
    return dt
