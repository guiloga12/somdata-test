from .contracts import HttpClient
from .utils import base_headers
from settings import BITTREX_SERVER_URL, BITTREX_PORT, BITTREX_BASE_API_URL


class BittrexHttpClient(HttpClient):
    COMMON_HEADERS = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
    }

    @classmethod
    def new(cls, server_url: str = BITTREX_SERVER_URL, port: int = BITTREX_PORT,
            base_api_url: str = BITTREX_BASE_API_URL):
        return cls(server_url, port, base_api_url, base_headers=cls.COMMON_HEADERS)

    @base_headers
    def get(self, *args, **kwargs):
        return super().get(*args, **kwargs)
