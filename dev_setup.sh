#!/bin/sh

# migrate database
python manage.py makemigrations
python manage.py migrate

# create superuser
echo "from django.contrib.auth import \
      get_user_model; User = get_user_model(); \
      User.objects.create_superuser('admin', 'admin@somdatatest.com', 'admin')" | python manage.py shell

echo "A development User has been created with:"
echo "username: admin"
echo "password: admin"

