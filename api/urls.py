from django.urls import path, include
from rest_framework import routers
from rest_framework.schemas import get_schema_view

from .views import MarketViewSet, MarketSummaryViewSet

api_router = routers.DefaultRouter()
api_router.register(r'markets', MarketViewSet)
api_router.register(r'summaries', MarketSummaryViewSet)


api_urls = [
    path('api/bittrex-exchanges/', include(api_router.urls)),
]

schema_urls =  [
    path('api/schema/',
         get_schema_view(title="Exchanges Portal",
                         description="Exposes BTC exchanges market data.",
                         version="1.0.0",
                         patterns=api_urls),
         name='openapi-schema'),
]

urlpatterns = api_urls + schema_urls
