from rest_framework import viewsets
from exchanges.models import MarketModel, MarketSummaryModel

from .serializers import MarketSerializer, MarketSummarySerializer


class MarketViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = MarketModel.objects.all()
    serializer_class = MarketSerializer
    filterset_fields = ['symbol', 'status', 'base_currency_symbol', 'quote_currency_symbol', ]


class MarketSummaryViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = MarketSummaryModel.objects.all()
    serializer_class = MarketSummarySerializer
    filterset_fields = ['market__symbol', ]
