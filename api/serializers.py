import json

from exchanges.models import MarketModel, MarketSummaryModel
from rest_framework import serializers


class JSONStringField(serializers.Field):
    """
    A JSON String serialized to a dict primitive.
    """
    def to_representation(self, obj) -> dict:
        return json.loads(obj)

    def to_internal_value(self, data) -> str:
        return json.dumps(data)


class MarketSerializer(serializers.ModelSerializer):
    prohibited_in = JSONStringField()
    associated_terms_of_service = JSONStringField()
    tags = JSONStringField()

    class Meta:
        model = MarketModel
        fields = '__all__'


class MarketSummarySerializer(serializers.ModelSerializer):
    market_symbol = serializers.SerializerMethodField()

    class Meta:
        model = MarketSummaryModel
        fields = '__all__'

    def get_market_symbol(self, obj):
        return obj.market.symbol
