from celery import shared_task
from common.clients import BittrexHttpClient
from exchanges.models import MarketModel, MarketSummaryModel

bittrex_client = BittrexHttpClient.new()


@shared_task
def fetch_all_markets():
    response = bittrex_client.get('markets')
    for item in response.json():
        MarketModel.create_from_dict(object_data=item)


@shared_task
def fetch_market_summary(symbol):
    """
    Task that fetches a market summary.
    It gets data from bittrex markets api and saves it into DB.
    """
    response = bittrex_client.get('markets/%s/summary' % symbol.upper())
    MarketSummaryModel.create_from_dict(object_data=response.json())
