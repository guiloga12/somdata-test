from django.contrib import admin
from .models import MarketModel, MarketSummaryModel


@admin.register(MarketModel)
class MarketAdmin(admin.ModelAdmin):
    list_display = ('uuid', 'symbol', 'min_trade_size', 'status', 'created_at', )
    list_filter = ('status', )
    ordering = ('symbol', )
    search_fields = ['uuid', 'symbol', ]


@admin.register(MarketSummaryModel)
class MarketSummaryAdmin(admin.ModelAdmin):
    list_display = ('uuid', 'market', 'percent_change', 'updated_at', )
    ordering = ('updated_at', 'saved_at', )
    search_fields = ['uuid', 'market__symbol', ]
