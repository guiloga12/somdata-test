from uuid import uuid4
import json

from django.db import models
from django.core.exceptions import ObjectDoesNotExist

from common.contracts import ModelContract
from common.utils import parse_dt
from exchanges.apps import ExchangeManagementConfig


class MarketModel(ModelContract):
    uuid = models.UUIDField(primary_key=True, default=uuid4, editable=False,
                            verbose_name='Resource Identifier')
    symbol = models.CharField(max_length=50, unique=True)
    base_currency_symbol = models.CharField(max_length=50)
    quote_currency_symbol = models.CharField(max_length=50)
    min_trade_size = models.CharField(max_length=50)
    precision = models.IntegerField()
    status = models.CharField(max_length=50, choices=[('online', 'ONLINE'),
                                                      ('offline', 'OFFLINE')],)
    created_at = models.DateTimeField()
    prohibited_in = models.TextField()
    associated_terms_of_service = models.TextField()
    tags = models.TextField()

    class Meta:
        db_table = f"{ExchangeManagementConfig.name}_market"

    @classmethod
    def create_from_dict(cls, object_data: dict):
        symbol = object_data.get('symbol')
        market_data = dict(
            symbol=symbol,
            base_currency_symbol=object_data.get('baseCurrencySymbol'),
            quote_currency_symbol=object_data.get('quoteCurrencySymbol'),
            min_trade_size=object_data.get('minTradeSize'),
            precision=object_data.get('precision'),
            status=object_data.get('status'),
            created_at=parse_dt(object_data.get('createdAt')),
            prohibited_in=json.dumps(object_data.get('prohibitedIn')),
            associated_terms_of_service=json.dumps(object_data.get('associatedTermsOfService')),
            tags=json.dumps(object_data.get('tags')), )

        mk = cls.find_one_by_symbol(symbol)
        if not mk:
            mk = cls(**market_data)
            mk.save()
        else:
            for key, value in market_data.items():
                setattr(mk, key, value)
                mk.save()

        return mk

    @classmethod
    def find_one_by_symbol(cls, symbol, raise_exc=False):
        try:
            mk = cls.objects.get(symbol=symbol)
        except ObjectDoesNotExist:
            if raise_exc:
                raise Exception(f"Market with symbol {symbol} doesn't exist.")
            mk = None

        return mk

    def __str__(self):
        return self.symbol


class MarketSummaryModel(ModelContract):
    uuid = models.UUIDField(primary_key=True, default=uuid4, editable=False,
                            verbose_name='Resource Identifier')
    market = models.ForeignKey(MarketModel, on_delete=models.CASCADE, related_name='summaries')
    saved_at = models.DateTimeField(auto_now_add=True)
    high = models.CharField(max_length=50)
    low = models.CharField(max_length=50)
    volume = models.CharField(max_length=50)
    quote_volume = models.CharField(max_length=50)
    percent_change = models.CharField(max_length=50)
    updated_at = models.CharField(max_length=50)

    @classmethod
    def create_from_dict(cls, object_data: dict):
        market = MarketModel.find_one_by_symbol(
            object_data.get('symbol'), raise_exc=True)

        market_summary = cls(
            uuid=object_data.get('uuid', uuid4()),
            market=market,
            high=object_data.get('high'),
            low=object_data.get('low'),
            volume=object_data.get('volume'),
            quote_volume=object_data.get('quoteVolume'),
            percent_change=object_data.get('percentChange'),
            updated_at=parse_dt(object_data.get('updatedAt')), )

        market_summary.save()
        return market_summary

    class Meta:
        db_table = f"{ExchangeManagementConfig.name}_market_summary"
        get_latest_by = ['updated_at', 'saved_at']

    def __str__(self):
        return "#{0} - [{2}] {1}".format(
            self.uuid, self.market.symbol, self.updated_at)
