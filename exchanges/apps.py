from django.apps import AppConfig


class ExchangeManagementConfig(AppConfig):
    name = 'exchanges'
