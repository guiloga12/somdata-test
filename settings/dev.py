from pathlib import Path
from .common import BASE_DIR

DEBUG = True

ALLOWED_HOSTS = [
    '127.0.0.1',
    'localhost',
]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'somdatatest',
        'USER': 'somdatauser',
        'PASSWORD': 'somdatapass',
        'HOST': 'localhost',
        'PORT': 5433,
    },
}
