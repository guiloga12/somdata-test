# Somdata Test - Exchanges

Simple django backend integrated with celery to poll BTC exchanges market data.

### Prerequisites ###

* Python version [**3.9**](https://www.python.org/downloads/release/python-390/) (
  with [pip](https://pip.pypa.io/en/stable/))
  
* Build with: [**Django 3.1**](https://docs.djangoproject.com/en/3.1)

### Setup ###

Ensure that python3.9 is installed in your machine and that python3.9 binary is in your PATH.

Install **poetry** for easier dependency management:
```
python3.9 -m pip install poetry
```

Environment installation (this creates a virtual environemnt and installs all development required packages):
```
poetry install
```

Run the PostgreSQL database and a RabbitMQ server with **docker-compose**:
```
docker-compose -f "docker-compose-dev.yml" up -d
```

Spawn a shell within the virtual environment and run the **dev_setup.sh** script:
```
poetry shell
sh dev_setup.sh
```

### Run on Development ###

Spawn a shell within the virtual environment and begin to work:

Run a **celery worker** and **celerybeat** to schedule tasks:
```
# Celery Worker
celery -A somdata_test worker -E -l INFO

# Celery Beat
celery -A somdata_test beat -l INFO --scheduler django_celery_beat.schedulers:DatabaseScheduler
```

Run the django development server:
```
python manage.py runserver
```

### Notes

* A superuser is created at setup for development with username: **admin** password: **admin**
* The **Base Api Url** is: [http://127.0.0.1:8000/api/bittrex-exchanges/](http://127.0.0.1:8000/api/bittrex-exchanges/).
* Take a look at the generated **OpenAPI Schema** at [http://127.0.0.1:8000/api/schema/](http://127.0.0.1:8000/openapi)

### Tests ###

* TODO

### Contributors ###

* Guillem López Garcia